package com.forthset.profinit.model;

import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class User {

    @Indexed(unique = true)
    private String username;

    private String password;
}
