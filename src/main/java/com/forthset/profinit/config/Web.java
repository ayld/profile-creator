package com.forthset.profinit.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;

@Configuration
public class Web {

    @Bean
    public RequestCache requestCache() {
        return new HttpSessionRequestCache();
    }
}
