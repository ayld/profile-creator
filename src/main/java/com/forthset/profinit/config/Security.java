package com.forthset.profinit.config;

import com.forthset.profinit.social.FacebookConnectionSignup;
import com.forthset.profinit.social.FacebookSignInAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.mem.InMemoryUsersConnectionRepository;
import org.springframework.social.connect.support.ConnectionFactoryRegistry;
import org.springframework.social.connect.web.ProviderSignInController;

@Configuration
@EnableWebSecurity
public class Security extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;
    private final FacebookConnectionSignup facebookConnectionSignup;

    @Autowired
    public Security(UserDetailsService userDetailsService, FacebookConnectionSignup facebookConnectionSignup) {
        this.userDetailsService = userDetailsService;
        this.facebookConnectionSignup = facebookConnectionSignup;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf().disable()
            .authorizeRequests()
            .antMatchers("/login*").permitAll()
            .anyRequest().authenticated()
        .and()
            .formLogin().loginPage("/login").permitAll();
    }

    @Bean
    public ProviderSignInController providerSignInController(
            @Autowired UsersConnectionRepository usersConnectionRepository,
            @Autowired ConnectionFactoryLocator connectionFactoryLocator
    ) {
        ((InMemoryUsersConnectionRepository) usersConnectionRepository).setConnectionSignUp(facebookConnectionSignup);

        final FacebookSignInAdapter signInAdapter = new FacebookSignInAdapter(new HttpSessionRequestCache());
        return new ProviderSignInController(connectionFactoryLocator, usersConnectionRepository, signInAdapter);
    }

    @Bean
    public ConnectionFactoryLocator connectionFactoryLocator() {
        return new ConnectionFactoryRegistry();
    }

    @Bean
    public UsersConnectionRepository usersConnectionRepository(@Autowired ConnectionFactoryLocator connectionFactoryLocator) {
        return new InMemoryUsersConnectionRepository(connectionFactoryLocator);
    }
}
