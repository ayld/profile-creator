package com.forthset.profinit.social;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.web.context.request.NativeWebRequest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class FacebookSignInAdapter implements SignInAdapter {

    private final RequestCache requestCache;

    public FacebookSignInAdapter(RequestCache requestCache) {
        this.requestCache = requestCache;
    }

    @Override
    public String signIn(String userId, Connection<?> connection, NativeWebRequest nativeWebRequest) {
        final UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userId, null, null);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        return extractOriginalUrl(nativeWebRequest);
    }

    private String extractOriginalUrl(NativeWebRequest request) {
        final HttpServletRequest nativeReq = request.getNativeRequest(HttpServletRequest.class);
        final HttpServletResponse nativeRes = request.getNativeResponse(HttpServletResponse.class);
        if (nativeReq == null) {
            return null;
        }
        final SavedRequest saved = requestCache.getRequest(nativeReq, nativeRes);
        if (saved == null) {
            return null;
        }
        requestCache.removeRequest(nativeReq, nativeRes);
        removeAuthAttributes(nativeReq.getSession(false));
        return saved.getRedirectUrl();
    }

    private void removeAuthAttributes(HttpSession session) {
        if (session == null) {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }
}
