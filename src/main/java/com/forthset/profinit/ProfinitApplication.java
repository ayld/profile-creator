package com.forthset.profinit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProfinitApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProfinitApplication.class, args);
	}

}
