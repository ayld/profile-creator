package com.forthset.profinit.rest;

import com.forthset.profinit.rest.model.UserDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserResource {

    @PostMapping
    public ResponseEntity<UserDto> create() {
        return ResponseEntity.ok().build();
    }

}
