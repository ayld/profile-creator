package com.forthset.profinit.dao;


import com.forthset.profinit.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {
}
